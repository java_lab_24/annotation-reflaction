package app.view;

import app.model.domain.Printer;
import app.model.service.ClassInfoGetter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainView {

  private Map<String, String> menu;
  private Map<String, Printable> methods;
  private static final Logger logger = LogManager.getLogger(MainView.class);
  private final Scanner scn = new Scanner(System.in);
  private String input;

  public MainView() {
    menu = new LinkedHashMap<>();
    menu.put("1", "Get class full info");
    menu.put("2", "Execute tasks");
    menu.put("3", "Quit");

    methods = new LinkedHashMap<>();
    methods.put("1", this::option1);
    methods.put("2", this::option2);
  }

  private void option1() {
    logger.trace("Getting info about " + Printer.class);
    ClassInfoGetter printerClass = new ClassInfoGetter(Printer.class);
    logger.info("Class fields");
    System.out.println(printerClass.getClassFields());
    logger.info("Class constructors");
    System.out.println(printerClass.getClassConstructors());
    logger.info("Class methods");
    System.out.println(printerClass.getClassMethods());
  }

  private void option2()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Class aClass = Printer.class;
    ClassInfoGetter printerClass = new ClassInfoGetter(aClass);
    logger.info("Class methods");
    System.out.println(printerClass.getClassMethods());
    logger.trace("Invoking methods of " + Printer.class + " in ascending order");
    Printer printer = new Printer();
    Method method1 = aClass.getMethod("print", String.class);
    logger.info("Invoke result of first print() method");
    method1.invoke(printer, "Hello from first print() method");

    String[] arguments = {"Hello", "Hey", "Hoy"};
    Method method2 = aClass.getMethod("print", String[].class);
    logger.info("Invoke result of second print()");
    method2.invoke(printer, new Object[]{arguments});

    Method method3 = aClass.getMethod("print", String.class, int.class);
    logger.info("Invoke result of third print()");
    method3.invoke(printer, "Hey from 4th method", 4);

    Method method4 = aClass.getMethod("print", int.class, String[].class);
    logger.info("Invoke result of fourth print()");
    method4.invoke(printer ,4, arguments);

  }

  private void printMenu() {
    int counter = 1;
    for (String s : menu.values()) {
      System.out.println(counter + ". " + s);
      counter++;
    }
  }

  public void show() {
    do {
      printMenu();
      input = scn.nextLine();
      try {
        methods.get(input).print();
      } catch (Exception e){}
    }while (!input.equals("3"));
  }
}
