package app.view;

import java.lang.reflect.InvocationTargetException;

@FunctionalInterface
public interface Printable {

  void print() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException;
}
