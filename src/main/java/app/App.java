package app;

import app.view.MainView;
import java.lang.reflect.InvocationTargetException;

public class App {

  public static void main(String[] args)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
    new MainView().show();
  }
}
