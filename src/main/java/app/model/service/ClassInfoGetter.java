package app.model.service;

import app.model.domain.MessageHolder;
import app.model.domain.MethodMeta;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class ClassInfoGetter {

  private Class aClass;
  private Field[] classFields;
  private Constructor[] classConstructors;
  private Method[] classMethods;

  public ClassInfoGetter(Class aClass) {
    this.aClass = aClass;
    classFields = this.aClass.getFields();
    classConstructors = this.aClass.getConstructors();
    classMethods = this.aClass.getDeclaredMethods();
  }

  public String getClassFields() {
    return toString(classFields);
  }

  public String getClassConstructors() {
    return toString(classConstructors);
  }

  public String getClassMethods() {
    return toString(classMethods);
  }

  private String toString(Field[] fields) {
    StringBuilder sb = new StringBuilder();
    for (Field f : fields) {
      sb.append(Modifier.toString(f.getModifiers()) + " "
          + f.getType() + " "
          + f.getName() + "\n");
    }
    return sb.toString();
  }

  private String toString(Constructor[] constructors) {
    StringBuilder sb = new StringBuilder();
    for (Constructor c : constructors) {
      sb.append(Modifier.toString(c.getModifiers()) + " "
          + c.getName() + " "
          + toString(c.getParameters()) + "\n");
    }
    return sb.toString();
  }

  public String toString(Parameter[] parameters) {
    StringBuilder sb = new StringBuilder();
    sb.append("(");
    for (Parameter p : parameters) {
      sb.append(p.getType() + " " + p.getName() + ", ");
    }
    sb.append(")");
    return sb.toString();
  }

  private String toString(Method[] methods) {
    StringBuilder sb = new StringBuilder();
    for (Method m : methods) {
      sb.append(toString(m.getAnnotations())
          + Modifier.toString(m.getModifiers()) + " "
          + m.getReturnType() + " "
          + m.getName() + " "
          + toString(m.getParameters()) + "\n\n");
    }
    return sb.toString();
  }

  private String toString(Annotation[] annotations) {
    StringBuilder sb = new StringBuilder();
    for (Annotation a : annotations) {
      if (a instanceof MethodMeta) {
        sb.append(a.annotationType() + " "
            + "name = " + ((MethodMeta) a).name() + " "
            + "params = " + ((MethodMeta) a).params() + " "
            + "returnValue = " + ((MethodMeta) a).returnValue() + "\n");
      }
      if (a instanceof MessageHolder) {
        sb.append(a.annotationType() + " "
            + "message = " + ((MessageHolder) a).message() + "\n");
      }
    }
    return sb.toString();
  }
}
