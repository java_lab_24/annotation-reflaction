package app.model.domain;

public class Printer {

  @MethodMeta(name = "print", params = {"String message"}, returnValue = "void")
  @MessageHolder
  public static void print(String message) {
    System.out.println(message);
  }

  @MethodMeta(name = "print", params = {"String message", "int times"}, returnValue = "void")
  @MessageHolder(message = "First message")
  public static void print(String message, int times) {
    for (int i = 0; i < times; i++) {
      System.out.println(message);
    }
  }

  @MethodMeta(name = "print", params = {"String... messages"}, returnValue = "void")
  @MessageHolder(message = "Second message")
  public static void print(String[] messages) {
    for (String s : messages) {
      System.out.println(s);
    }
  }

  @MethodMeta(name = "print", params = {"int times", "String... messages"}, returnValue = "void")
  @MessageHolder(message = "Third message")
  public static void print(int times, String[] messages) {
    for (int i = 0; i < times; i++) {
      for (String s : messages) {
        System.out.println(s);
      }
    }
  }
}
